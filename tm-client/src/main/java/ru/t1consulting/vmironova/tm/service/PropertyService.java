package ru.t1consulting.vmironova.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['buildNumber']}")
    private String applicationVersion;

    @Value("#{environment['server.port']}")
    private String port;

    @Value("#{environment['server.host']}")
    private String host;

    @Value("#{environment['admin.login']}")
    private String adminLogin;

    @Value("#{environment['admin.password']}")
    private String adminPassword;

}
