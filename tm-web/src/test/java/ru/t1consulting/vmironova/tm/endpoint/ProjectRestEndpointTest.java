package ru.t1consulting.vmironova.tm.endpoint;

import org.junit.experimental.categories.Category;
import ru.t1consulting.vmironova.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public final class ProjectRestEndpointTest {

    /*@NotNull
    private final ProjectEndpoint projectEndpointClient = ProjectEndpoint.client();

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO(USER_PROJECT2_NAME, USER_PROJECT2_DESCRIPTION);

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO(USER_PROJECT3_NAME, USER_PROJECT3_DESCRIPTION);

    private long baseCount = 0;

    @Before
    public void before() throws Exception {
        baseCount = projectEndpointClient.findAll().size();
        projectEndpointClient.add(project1);
        projectEndpointClient.add(project2);
    }

    @After
    public void after() throws Exception {
        projectEndpointClient.delete(project1);
        projectEndpointClient.delete(project2);
        projectEndpointClient.delete(project3);
    }

    @Test
    public void testFindAll() throws Exception {
        @Nullable final List<ProjectDTO> projects = projectEndpointClient.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(baseCount + 2, projects.size());
        for (@NotNull ProjectDTO project : projects) {
            Assert.assertNotNull(projectEndpointClient.findById(project.getId()));
        }
    }

    @Test
    public void testAdd() throws Exception {
        @Nullable ProjectDTO project = projectEndpointClient.add(project3);
        Assert.assertNotNull(project);
        Assert.assertEquals(project3.getName(), project.getName());
        Assert.assertEquals(project3.getDescription(), project.getDescription());
    }

    @Test
    public void testSave() throws Exception {
        @Nullable ProjectDTO project = projectEndpointClient.findById(project1.getId());
        project.setStatus(Status.IN_PROGRESS);
        Assert.assertNotNull(projectEndpointClient.save(project));
        @Nullable ProjectDTO project2 = projectEndpointClient.findById(project1.getId());
        Assert.assertEquals(project.getStatus(), project2.getStatus());
    }

    @Test
    public void testFindById() throws Exception {
        @Nullable ProjectDTO project = projectEndpointClient.findById(project1.getId());
        Assert.assertEquals(USER_PROJECT1_NAME, project.getName());
        Assert.assertEquals(USER_PROJECT1_DESCRIPTION, project.getDescription());
    }

    @Test
    public void testExistsById() throws Exception {
        Assert.assertTrue(projectEndpointClient.existsById(project1.getId()));
        Assert.assertFalse(projectEndpointClient.existsById(project3.getId()));
    }

    @Test
    public void testCount() throws Exception {
        Assert.assertEquals(baseCount + 2, projectEndpointClient.count());
    }

    @Test
    public void testDeleteById() throws Exception {
        projectEndpointClient.deleteById(project1.getId());
        Assert.assertNull(projectEndpointClient.findById(project1.getId()));
    }

    @Test
    public void testDelete() throws Exception {
        projectEndpointClient.delete(project1);
        Assert.assertNull(projectEndpointClient.findById(project1.getId()));
    }*/

}
