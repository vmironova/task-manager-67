package ru.t1consulting.vmironova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1consulting.vmironova.tm.api.endpoint.ProjectEndpoint;
import ru.t1consulting.vmironova.tm.api.service.dto.IProjectDTOService;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1consulting.vmironova.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private IProjectDTOService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() throws Exception {
        return projectService.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/add")
    public ProjectDTO add(
            @WebParam(name = "project", partName = "project")
            @RequestBody final @NotNull ProjectDTO project
    ) throws Exception {
        return projectService.add(project);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public ProjectDTO save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final @NotNull ProjectDTO project
    ) throws Exception {
        return projectService.update(project);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) throws Exception {
        return projectService.findOneById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) throws Exception {
        ProjectDTO p = projectService.findOneById(id);
        return (projectService.findOneById(id) != null);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws Exception {
        return projectService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) throws Exception {
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final @NotNull ProjectDTO project
    ) throws Exception {
        projectService.remove(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void clear() throws Exception {
        projectService.clear();
    }

}
