package ru.t1consulting.vmironova.tm.exception.field;

public final class EmailEmptyExceprion extends AbstractFieldException {

    public EmailEmptyExceprion() {
        super("Error! Email is empty.");
    }

}
